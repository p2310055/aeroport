#ifndef DASHBOARDD_H
#define DASHBOARDD_H

#include <QDialog>

namespace Ui {
class dashboardd;
}

class dashboardd : public QDialog
{
    Q_OBJECT

public:
    explicit dashboardd(QWidget *parent = nullptr);
    ~dashboardd();

private:
    Ui::dashboardd *ui;
};

#endif // DASHBOARDD_H
