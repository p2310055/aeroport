#ifndef ANESANTOINE1_DATE_H
#define ANESANTOINE1_DATE_H
#include <QString>
#include <string>
using namespace std;
class Date {
private:
    unsigned int jour;
    unsigned int mois;
    unsigned int annee;
    unsigned int heure;
public:
    Date();
    Date(unsigned int _jour, unsigned int _mois, unsigned int _annee, unsigned int _heure);
    Date(unsigned int _jour, unsigned int _mois, unsigned int _annee);

    void setHeure(const unsigned int nvHeure);
    void setJour(const unsigned int nvJour);
    void setMois(const unsigned int nvMois);
    void setAnnee(const unsigned int nvAnnee);
    // to qsting:
    QString qdatetoString() const;
    string dateToString(const Date d1)const;

    unsigned int getHeure() const;
    unsigned int getJour() const;
    unsigned int getMois() const;
    unsigned int getAnnee() const;

};
#endif //ANESANTOINE1_DATE_H
