#ifndef CLIENT_H
#define CLIENT_H
#include <string>

/*#include <QMainWindow>

class Client : public QMainWindow
{
    Q_OBJECT
public:
    explicit Client(QWidget *parent = nullptr);

signals:
};
*/
using namespace std;
class Client {
private:
    string nom;
    string prenom;
    int age;
public:
    Client(string _nom,string _prenom,int _age);
    int getage();
};


#endif // CLIENT_H
