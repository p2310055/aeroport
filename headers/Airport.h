#ifndef ANESANTOINE1_AIRPORT_H
#define ANESANTOINE1_AIRPORT_H

#include <unordered_map>
#include <string>
#include "Customer.h"
#include "Plane.h"
#include "Flight.h"
#include <QString>

using namespace std;

class Airport {
private:
    unordered_map<string, Customer> customers;
    unordered_map<string, Plane> planes;
    unordered_map<string, Flight> flights;

public:
    static Airport& getInstance();

    // Customer
    void addCustomer(const QString& _name, const QString& _forName, const QString& _birthDate, const QString& _email, const QString& _phone);

    void removeCustomer(const string& _id);

    Customer& getCustomer(const string& _id);

    // Plane
    void addPlane(const QString _name,const QString _type,const QString _state,const QString _seatsnumber );

    void removePlane(const string& _id);

    Plane& getPlane(const string& _id);

    // Flight
    // suprime const QString& _planeId,  de la fonction add flight
    void addFlight(const QString& _departure, const QString& _arrival, const QString& _departureDate, const QString& _arrivalDate,  const QString &_price,const QString _plane,const QString _class);

    void removeFlight(const string& _id);

    Flight& getFlight(const string& _id);

    // Reservation
    unsigned int getPriceFor(const string& customerId, const string& flightId, const unsigned int& seats);

    void makeReservation(const string& customerId, const string& flightId, const unsigned int& seats);
};
#endif //ANESANTOINE1_AIRPORT_H
