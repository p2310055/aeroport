#ifndef PAGEAC_H
#define PAGEAC_H
//#include "headers/editcustomer.h"
#include <QDialog>
#include <QTextEdit>
#include <QPushButton>
#include "customerinfo.h"
#include "headers/addflight.h"
#include "headers/database.h"
#include "headers/addplane.h"
#include "headers/dashboardd.h"
namespace Ui {
class pageac;
}

class pageac : public QDialog
{
    Q_OBJECT

public:
    explicit pageac(QWidget *parent = nullptr);
    ~pageac();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_test_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_4_clicked();

private:
    Ui::pageac *ui;
    QTextEdit *textEdit; // Déclaration de textEdit ici
    QPushButton *addClientButton;
    customerInfo *custinfo;
    addFlight * addflightpage;
    addPlane * addpl;
    dashboardd * dash;

};

#endif // PAGEAC_H
