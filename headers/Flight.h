#ifndef ANESANTOINE1_FLIGHT_H
#define ANESANTOINE1_FLIGHT_H

#include <string>
#include "headers/Date.h"

using namespace std;

class Flight {
private:
    string id;
    string departure;
    string arrival;
    string planeId;
    Date departureDate;
    Date arrivalDate;
    unsigned int reservedSeats;
    unsigned int price;

public:
    Flight(const string& _id, const string& _departure, const string& _arrival, const string& _planeId, const Date& _departureDate, const Date& _arrivalDate, const unsigned int& _reservedSeats, const unsigned int& _price);

    // Getter
    string getId() const;

    string getDeparture() const;

    string getArrival() const;

    string getPlaneId() const;

    Date getDepartureDate() const;

    Date getArrivalDate() const;

    unsigned int getReservedSeats() const;

    unsigned int getPrice() const;

    // Setter
    void addHourLate(const unsigned int& _late);

    void reserveSeats(const unsigned int& _seats);

    void cancelSeats(const unsigned int& _seats);

    void setPrice(const unsigned int& _price);

    void setPlaneId(const string& _planeId);
};
#endif //ANESANTOINE1_FLIGHT_H

