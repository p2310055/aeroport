#ifndef DATABASE_H
#define DATABASE_H
#include <QDebug>
#include <QtSql>
#include <QString>
#include <string>
#include "headers/Date.h"
using namespace std;
class Database
{
public:
    //constructeur pour connecter :
    Database();
    void checkdb();
    void createCustomer(QString Id,QString First,QString Last,QString Birth_date,QString Phone,QString Email,QString Registration_date,int Fidpoint);
    void createFlight(QString idF,QString departure,QString arrival,QString departureDate,QString arrivalDate,QString price, QString _plane, QString _class);
    void createPlane(QString idP,QString name,QString type,QString state,QString seatsumber);
    void closedb();
private:
    QSqlDatabase my_db;

};

#endif // DATABASE_H

