#ifndef ADDFLIGHT_H
#define ADDFLIGHT_H

#include <QDialog>
#include "headers/Airport.h"
namespace Ui {
class addFlight;
}

class addFlight : public QDialog
{
    Q_OBJECT

public:
    explicit addFlight(QWidget *parent = nullptr);
    ~addFlight();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::addFlight *ui;
};

#endif // ADDFLIGHT_H
