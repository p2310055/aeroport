#ifndef ANESANTOINE1_PLANE_H
#define ANESANTOINE1_PLANE_H

#include <string>
#include "headers/Date.h"

using namespace std;

enum class PlaneType {
    AIRBUS_A320,
    AIRBUS_A330,
    AIRBUS_A350,
    BOEING_737,
    BOEING_747,
    BOEING_777,
    BOEING_787
};

enum class PlaneState {
    AVAILABLE,
    MAINTENANCE
};

class Plane {
private:
    string id;
    string name;
    PlaneType type;
    PlaneState state;
    unsigned int totalSeats;

public:
    Plane(const string &_id, const PlaneType &_type, const PlaneState &_state, const unsigned int &_totalSeats);

    // Getter
    string getId() const;

    PlaneType getType() const;

    PlaneState getState() const;

    unsigned int getTotalSeats() const;
};
#endif //ANESANTOINE1_PLANE_H
