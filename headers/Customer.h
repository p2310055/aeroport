#ifndef ANESANTOINE1_CUSTOMER_H
#define ANESANTOINE1_CUSTOMER_H
#include <string>
#include "headers/Date.h"
#include "Ticket.h"
#include <QString>
using namespace std;

class Customer {
private:
    string id;
    string name;
    string forName;
    string email;
    string phone;

    Date birthDate;
    Date registrationDate;

    unsigned int fidelityScore;

    Ticket* ticket = nullptr; // todo: make possible to have multiple tickets
public:
    Customer(const string& _name, const string& _forName, const Date& _birthDate, const string& _email, const string& _phone);

    // Getter
    string getId() const;

    string getName() const;

    string getForName() const;

    string getEmail() const;

    string getPhone() const;

    Date getBirthDate() const;

    Date getRegistrationDate() const;

    unsigned int getFidelityScore() const;

    Ticket getTicket() const;
    static string makeId(const string &nom,const string &prenom);
    // Setter
    void setEmail(const string& _email);

    void setPhone(const string& _phone);

    void addFidelityScore();

    void setFidelityScore(unsigned int _fidelityScore);

    void setTicket(Ticket * _ticket);

};
#endif //ANESANTOINE1_CUSTOMER_H
