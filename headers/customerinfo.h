#ifndef CUSTOMERINFO_H
#define CUSTOMERINFO_H

#include <QDialog>

namespace Ui {
class customerInfo;
}

class customerInfo : public QDialog
{
    Q_OBJECT

public:
    explicit customerInfo(QWidget *parent = nullptr);
    ~customerInfo();

private slots:
    void on_pushButton_submitecustomer_clicked();

    void on_pushButton_cancel_clicked();

private:
    Ui::customerInfo *ui;
};

#endif // CUSTOMERINFO_H
