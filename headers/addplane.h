#ifndef ADDPLANE_H
#define ADDPLANE_H

#include <QDialog>

namespace Ui {
class addPlane;
}

class addPlane : public QDialog
{
    Q_OBJECT

public:
    explicit addPlane(QWidget *parent = nullptr);
    ~addPlane();

private slots:
    void on_pushButton_clicked();

private:
    Ui::addPlane *ui;

};

#endif // ADDPLANE_H
