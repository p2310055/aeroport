#ifndef ANESANTOINE1_TICKET_H
#define ANESANTOINE1_TICKET_H

#include <string>

using namespace std;

class Ticket {
private:
    string flightId;
    unsigned int seats;
public:
    Ticket(const string& _flightId, const unsigned int& _seats);

    // Getter
    string getFlightId() const;

    unsigned int getSeats() const;

    // Setter
    void setSeats(const unsigned int& _seats);

    void addSeats(const unsigned int& _seats);

    void removeSeats(const unsigned int& _seats);
};
#endif //ANESANTOINE1_TICKET_H