#include <string>
#include "headers/Date.h"
#include "headers/Customer.h"
#include <chrono>
#include <iostream>
#include <QString>
using namespace std;

string Customer::makeId(const string &nom,const string &prenom){
    auto currentTime = chrono::system_clock::now().time_since_epoch();
    long idnum = chrono::duration_cast<chrono::seconds>(currentTime).count();
    if(nom.empty() || prenom.empty()){
        return to_string(idnum);
    }
    char nomInitial= toupper(nom[0]);
    char prenomInitial= toupper(prenom[0]);
    string initiaux = string(1,nomInitial)+string(1,prenomInitial);

    return initiaux + to_string(idnum);
}
Customer::Customer( const string& _name, const string& _forName, const Date& _birthDate, const string& _email, const string& _phone) {

    //string id =makeId(_name,_forName);
    name = _name;
    forName = _forName;
    birthDate = _birthDate;
    registrationDate = Date();
    email = _email;
    phone = _phone;
    fidelityScore = 0;
    cout<<id;
}

string Customer::getId() const {
    return id;
}

string Customer::getName() const {
    return name;
}

string Customer::getForName() const {
    return forName;
}

string Customer::getEmail() const {
    return email;
}

string Customer::getPhone() const {
    return phone;
}

Date Customer::getBirthDate() const {
    return birthDate;
}

Date Customer::getRegistrationDate() const {
    return registrationDate;
}

unsigned int Customer::getFidelityScore() const {
    return fidelityScore;
}

Ticket Customer::getTicket() const {
    return *ticket;
}

void Customer::setEmail(const string& _email) {
    email = _email;
}

void Customer::setPhone(const string& _phone) {
    phone = _phone;
}

void Customer::addFidelityScore() {
    fidelityScore++;
}

void Customer::setFidelityScore(unsigned int _fidelityScore) {
    fidelityScore = _fidelityScore;
}

void Customer::setTicket(Ticket * _ticket) {
    ticket = _ticket;
}

