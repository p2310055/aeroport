#include "headers/customerinfo.h"
#include "ui_customerinfo.h"
#include "headers/Airport.h"
#include <string>
#include <QWidget>
#include <iostream>
#include <QTextEdit>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QSqlQuery>
#include "headers/pageac.h"
#include <QMessageBox>
using namespace std;


customerInfo::customerInfo(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::customerInfo)
{
    ui->setupUi(this);
    // QString headerStyle = "border-raduis:30%";

    // ui->pushButton_submitecustomer->setStyleSheet(headerStyle);
    // QString headerStyle = "border: 2px solid black;"
    //                       "border-radius: 30px;";

    // ui->pushButton_submitecustomer->setStyleSheet(headerStyle);



}

customerInfo::~customerInfo()
{
    delete ui;
}

void customerInfo::on_pushButton_submitecustomer_clicked()
{
    QString firstname = ui->lineEdit_firstname->text();
    //string firstName = firstname.toStdString();
    QString lastname = ui->lineEdit_lastname->text();
    //string lastName = lastname.toStdString();
    auto phonenum = ui->lineEdit_phone->text();
    //string phoneNum = phonenum.toStdString();
    auto  email = ui->lineEdit_mail->text();
    //string eMail = email.toStdString();
    // crée un objet Date
    auto birthdate = ui->dateEdit_birthdate->text();
    // int year1 = birthdate.year();
    // int month1 = birthdate.month();
    // int day1 = birthdate.day();
    // Date birthDate(day1,month1,year1);

    if (firstname.isEmpty() ||  lastname.isEmpty() || phonenum.isEmpty() ||
        email.isEmpty() ||  birthdate.isEmpty()){
            QMessageBox::information(this,"Login","information missing");
    }

    //Airport airport = Airport();
    Airport airport = Airport::getInstance();

    //airport.addPlane(PlaneType::AIRBUS_A320, PlaneState::AVAILABLE, 150);
    airport.addCustomer(firstname, lastname, birthdate, email, phonenum);
    // Inclure le fichier d'en-tête de la fenêtre principale

    hide();




}


void customerInfo::on_pushButton_cancel_clicked()
{
    hide();
}

