#include "headers/Plane.h"

Plane::Plane(const string &_id, const PlaneType &_type, const PlaneState &_state, const unsigned int &_totalSeats) {
    id = _id;
    type = _type;
    state = _state;
    totalSeats = _totalSeats;
}

string Plane::getId() const {
    return id;
}

PlaneType Plane::getType() const {
    return type;
}

PlaneState Plane::getState() const {
    return state;
}

unsigned int Plane::getTotalSeats() const {
    return totalSeats;
}
