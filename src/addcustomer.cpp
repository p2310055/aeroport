#include "headers/addcustomer.h"
#include "ui_addcustomer.h"
#include <iostream>
#include <QString>
#include "headers/Airport.h"
using namespace std;
addCustomer::addCustomer(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::addCustomer)
{
    ui->setupUi(this);
}

addCustomer::~addCustomer()
{
    delete ui;
}



void addCustomer::on_SubmitClient_clicked()
{
    Airport airport = Airport();
    airport.addCustomer("FOUR", "Antoine", Date(1999, 1, 1), "antoine.four@etu.univ-lyon1.fr", "0123456789");
}

