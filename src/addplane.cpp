#include "headers/addplane.h"
#include "ui_addplane.h"
#include <QMessageBox>
#include <headers/Airport.h>
addPlane::addPlane(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::addPlane)
{
    ui->setupUi(this);
    QComboBox * planechoice = ui->comboBox;
    planechoice->addItem("select a plane");
    planechoice->addItem("AIRBUS_A320");
    planechoice->addItem("AIRBUS_A330");
    planechoice->addItem("AIRBUS_A350");
    planechoice->addItem("BOEING_737");
    planechoice->addItem("BOEING_747");
    planechoice->addItem("BOEING_777");
    planechoice->addItem("BOEING_787");
    QComboBox * classchoice = ui->comboBox_2;
    classchoice->addItem("select a state");
    classchoice->addItem("available");
    classchoice->addItem("maintenance");
}

addPlane::~addPlane()
{
    delete ui;
}

void addPlane::on_pushButton_clicked()
{
    qDebug()<<"hh";
    QString pname = ui->lineEdit_name->text();
    QString pseats = ui->lineEdit_seats->text();
    QString ptype = ui->comboBox->currentText();
    QString pstate = ui->comboBox_2->currentText();

    if (pname.isEmpty() ||  pseats.isEmpty() || ptype.contains("select a plane") || pstate.contains("select a state")
         ){
        QMessageBox::information(this,"plane","information missing");

    }
    //Airport airport = Airport();
    Airport airport = Airport::getInstance();
    airport.addPlane(pname,pseats,ptype,pstate);

}

