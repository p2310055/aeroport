#include "headers/Flight.h"

Flight::Flight(const string &_id, const string &_departure, const string &_arrival, const string &_planeId,
               const Date &_departureDate, const Date &_arrivalDate, const unsigned int &_reservedSeats,
               const unsigned int &_price) {
    id = _id;
    departure = _departure;
    arrival = _arrival;
    planeId = _planeId;
    departureDate = _departureDate;
    arrivalDate = _arrivalDate;
    reservedSeats = _reservedSeats;
    price = _price;
}

string Flight::getId() const {
    return id;
}

string Flight::getDeparture() const {
    return departure;
}

string Flight::getArrival() const {
    return arrival;
}

string Flight::getPlaneId() const {
    return planeId;
}

Date Flight::getDepartureDate() const {
    return departureDate;
}

Date Flight::getArrivalDate() const {
    return arrivalDate;
}

unsigned int Flight::getReservedSeats() const {
    return reservedSeats;
}

unsigned int Flight::getPrice() const {
    return price;
}

void Flight::addHourLate(const unsigned int &_late) {
    departureDate.setHeure(_late + departureDate.getHeure());
    arrivalDate.setHeure(_late + arrivalDate.getHeure());
}

void Flight::reserveSeats(const unsigned int &_seats) {
    reservedSeats += _seats;
}

void Flight::cancelSeats(const unsigned int &_seats) {
    reservedSeats -= _seats;
}

void Flight::setPrice(const unsigned int &_price) {
    price = _price;
}

void Flight::setPlaneId(const string &_planeId) {
    planeId = _planeId;
}
