#include "headers/Airport.h"
#include <string>
#include <iostream>
#include <chrono>
#include <QDate>
#include "headers/Customer.h"
#include "headers/database.h"
#include "headers/Date.h"
#include <QString>
using namespace std;
QString obtenirDateAujourdhui() {
    // Obtenir la date actuelle
    QDate date = QDate::currentDate();

    // Formater la date selon le format requis
    QString dateFormatee = date.toString("dd/MM/yyyy");

    return dateFormatee;
}

Airport& Airport::getInstance(){
    static Airport instance;
    return instance;
}

void Airport::addCustomer(const QString &_name, const QString &_forName, const QString &_birthDate, const QString &_email, const QString &_phone) {
    string txtname = _name.toStdString();
    string txtforname = _forName.toStdString();
    string txtid =  Customer::makeId(txtname,txtforname);
    QString id = QString::fromStdString(txtid);
    //string id =  Customer::makeId(_name.toStdString(), _forName.toStdString());
    //QString id = "EA13434";
    //customers.insert({id, Customer(_name, _forName, _birthDate, _email, _phone)});
    Database my_db;
    // string dateenTxt = _birthDate.dateToString(_birthDate);
    // QString dyy = QString::fromStdString(dateenTxt);
    my_db.createCustomer(id,_name,_forName,_birthDate,_phone,_email,obtenirDateAujourdhui(),0);
    //                      dyy,
    //                      QString::fromStdString(_email),
    //                      QString::fromStdString(_phone),"",0);
    //cout << "Customer " << _name << " " << _forName << " added." << endl;
    cout<<"customer added"<<endl;



}

void Airport::removeCustomer(const string &_id) {
    customers.erase(_id);
}

Customer &Airport::getCustomer(const string &_id) {
    return customers.at(_id);
}

void Airport::addPlane(const QString _name,const QString _type,const QString _state,const QString _seatsnumber ) {
    string txtname = _name.toStdString();
    string txttype = _type.toStdString();
    string txtid =  Customer::makeId(txtname,txttype);
    QString id = QString::fromStdString(txtid);
    Database my_db;
    my_db.createPlane(id,_name,_type,_state,_seatsnumber);
    //string id = to_string(planes.size());
    //planes.insert({id, Plane(id, _type, _state, _totalSeats)});

    //cout << "Plane " << id << " added." << endl;
}

void Airport::removePlane(const string &_id) {
    planes.erase(_id);
}

Plane &Airport::getPlane(const string &_id) {
    return planes.at(_id);
}

void Airport::addFlight(const QString &_departure, const QString &_arrival,const QString &_departureDate, const QString &_arrivalDate, const  QString &_price,const QString _plane,const QString _class) {
    string txtdepart = _departure.toStdString();
    string txtarriv = _arrival.toStdString();
    string txtid =  Customer::makeId(txtdepart,txtarriv);
    QString id = QString::fromStdString(txtid);
    QString dateinscription = obtenirDateAujourdhui();
    // string departure = _departure.toStdString();
    // string arrival = _arrival.toStdString();
    // string departureDate = _departureDate.toStdString();
    // string arrivalDate = _arrivalDate.toStdString();
    // //string price = _price.toStdString();
    qDebug()<<id;
    Database my_db;

    my_db.createFlight(id,_departure,_arrival,_departureDate,_arrivalDate,_price,_plane,_class);

    // string id = to_string(flights.size());
    // flights.insert({id, Flight(id, _departure, _arrival, _planeId, _departureDate, _arrivalDate, 0, _price)});
    // cout << "Flight " << id << " added." << endl;
    qDebug()<<"flight added";

}

void Airport::removeFlight(const string &_id) {
    flights.erase(_id);
}

Flight &Airport::getFlight(const string &_id) {
    return flights.at(_id);
}

unsigned int Airport::getPriceFor(const string &customerId, const string &flightId, const unsigned int &seats) {
    unsigned int price = 0;

    Flight flight = flights.at(flightId);

    for (int i = 0; i < seats; i++) {
        price += flight.getPrice();
    }
    Customer customer = customers.at(customerId);

    const unsigned int fidelityScore = customer.getFidelityScore();

    if (fidelityScore > 0) {
        price /= fidelityScore;
    }
    return price;
}

void Airport::makeReservation(const string &customerId, const string &flightId, const unsigned int &seats) {
    const unsigned int price = getPriceFor(customerId, flightId, seats);
    Customer &customer = customers.at(customerId);

    customer.addFidelityScore();

    Flight &flight = flights.at(flightId);

    flight.reserveSeats(seats);

    Ticket *ticket = new Ticket(flightId, seats);

    customer.setTicket(ticket);

    cout << "Reservation made for " << customer.getName() << " " << customer.getForName() << " for " << seats << " seats for a total of " << price << "EUR." << endl;
}
