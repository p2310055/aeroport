
#include "headers/addflight.h"
#include "ui_addflight.h"
#include <QMessageBox>
#include "headers/Airport.h"
addFlight::addFlight(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::addFlight)
{
    ui->setupUi(this);
    QComboBox * planechoice = ui->comboBox_plane;
    planechoice->addItem("select a plane");
    planechoice->addItem("AIRBUS_A320");
    planechoice->addItem("AIRBUS_A330");
    planechoice->addItem("AIRBUS_A350");
    planechoice->addItem("BOEING_737");
    planechoice->addItem("BOEING_747");
    planechoice->addItem("BOEING_777");
    planechoice->addItem("BOEING_787");
    QComboBox * classchoice = ui->comboBox;
    classchoice->addItem("select a class");
    classchoice->addItem("First Class");
    classchoice->addItem("Second Class");
    classchoice->addItem("Economique");


}

addFlight::~addFlight()
{
    delete ui;
}

void addFlight::on_pushButton_2_clicked()
{


}


void addFlight::on_pushButton_clicked()
{
    // recuperation des données :
    QString arrival = ui->lineEdit_arrival->text();
    QString departure = ui->lineEdit_departure->text();
    QString arrivalDate = ui->dateTimeEdit_arrival->text();
    QString depatureDate = ui->dateTimeEdit_departure->text();
    QString price = ui->lineEdit_price->text();
    QString plane = ui->comboBox_plane->currentText();
    QString fclass = ui->comboBox->currentText();
    // verification :
    if (arrival.isEmpty() ||  departure.isEmpty() || arrivalDate.isEmpty() ||
        depatureDate.isEmpty() ||  price.isEmpty()||plane.contains("select a plane")){
        QMessageBox::information(this,"Login","information missing");

    }

    Airport airport = Airport::getInstance();

    // Airport airport = Airport();
    //airport.addFlight("Oran","Lyon","20/10/2024","06/12/2024",2000);
    airport.addFlight(departure,arrival,depatureDate,depatureDate,price,plane,fclass);
        //(QString departure,QString arrival,QString departureDate,QString arrivalDate,QString price,QString plane,QString class_av)
    //(const QString &_departure, const QString &_arrival, const QString &_departureDate, const QString &_arrivalDate, const  double &_price)
    //airport.addFlight("");
    //airport.addFlight()



}

