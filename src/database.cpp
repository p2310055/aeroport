#include "headers/database.h"
//#include <string>
#include "headers/Date.h"
using namespace std;



Database::Database(){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
     db.setDatabaseName("C:/Users/Anes/Documents/database/techa.sqlite");
    if (!db.open()) {
         qDebug() << "Erreur lors de la connexion à la base de données";

     }else{
         qDebug()<<"base de donnée connectée";
     }
}
void Database::createCustomer(QString Id,QString First,QString Last,QString Birth_date,QString Phone,QString Email,QString Registration_date,int Fidpoint){
    QSqlQuery query;
    query.prepare("INSERT INTO customer(id,first_name,last_name,birth_date,email,phone,regestration_date,fidelity_score)"
                  "VALUES(:id, :first_name, :last_name, :birth_date, :email, :phone, :regestration_date, :fidelity_score)");
    query.bindValue(":id",Id);
    query.bindValue(":first_name",First);
    query.bindValue(":last_name",Last);

    // string _BdateJ= to_string(Birth_date.getJour());
    // string _BdateM= to_string(Birth_date.getMois());
    // string _BdateA= to_string(Birth_date.getAnnee());
    // string _Bdate = _BdateJ +"/" + _BdateM +"/" +_BdateA;
    // QString datyy = QString::fromStdString(_Bdate);
    query.bindValue(":birth_date",Birth_date);
    query.bindValue(":email",Email);
    query.bindValue(":phone",Phone);
    // string _RdateJ= to_string(Registration_date.getJour());
    // string _RdateM= to_string(Registration_date.getMois());
    // string _RdateA= to_string(Registration_date.getAnnee());
    // string _Rdate = _RdateJ +"/" + _RdateM +"/" +_RdateA;
    // QString Rdatyy = QString::fromStdString(_Rdate);

    query.bindValue(":regestration_date",Registration_date);
    query.bindValue(":fidelity_score",Fidpoint);
    if(!query.exec()){
        qDebug()<<"erreur lors de l'ajout du client"<<query.lastError().text();

    }else {
        qDebug()<<"client ajouté ! ";
    }


};
//addFlight(const QString &_departure, const QString &_arrival, const QString &_departureDate, const QString &_arrivalDate, const  double &_price)

void Database::createFlight(QString idF,QString departure,QString arrival,QString departureDate,QString arrivalDate,QString price,QString plane,QString class_av){
    // QSqlQuery query;
    // query.prepare("INSERT INTO flight(departure,arrival,planeId,date_departure,date_arrival,seat_reserved,price)"
    //               "VALUES(:departure, :arrival, :departureDate, :arrivalDate, :price)");
    // query.bindValue(":departure",departure);
    // query.bindValue(":arrival",arrival);
    // query.bindValue(":departureDate",departureDate);
    // query.bindValue(":arrivalDate",arrivalDate);
    // query.bindValue(":price",price);
    // if(!query.exec()){
    //     qDebug()<<"erreur lors de l'ajout du Flight"<<query.lastError().text();

    // }else {
    //     qDebug()<<"Flight ajouté aahhhhhhhhhhhhhh ! ";
    // }
    // QSqlQuery qry2;
    // qry2.prepare("SELECT idP FROM plane WHERE type = 'AIRBUS_A320' " );
    // qry2.bindValue(":plane", _plane);
    // QString _idP = qry2.value(0).toString();

    QSqlQuery query1;

    query1.prepare("INSERT INTO flight(idF,departure,arival,date_departure,date_arrival,price,plane_type,class)"
                  "VALUES(:idF, :departure, :arival, :date_departure, :date_arrival, :price, :plane_type, :class)");
    query1.bindValue(":idF",idF);
    query1.bindValue(":departure",departure);
    query1.bindValue(":arival",arrival);
    query1.bindValue(":date_departure",departureDate);
    query1.bindValue(":date_arrival",arrivalDate);
    query1.bindValue(":price",price);
    query1.bindValue(":plane_type",plane);
    query1.bindValue(":class",class_av);
    if(!query1.exec()){
        qDebug()<<"erreur lors de l'ajout du flight"<<query1.lastError().text();

    }else {
        qDebug()<<"client ajouté  ! ";
    }


}
void Database::createPlane(QString idP,QString name,QString type,QString state,QString seatsumber){
    QSqlQuery query1;

    query1.prepare("INSERT INTO plane(idP,name,type,state,totalSeats)"
                   "VALUES(:idF, :name, :type, :state, :totalSeats)");
    query1.bindValue(":idP",idP);
    query1.bindValue(":name",name);
    query1.bindValue(":type",type);
    query1.bindValue(":state",state);
    query1.bindValue(":totalSeats",seatsumber);
    if(!query1.exec()){
        qDebug()<<"erreur lors de l'ajout du plane"<<query1.lastError().text();

    }else {
        qDebug()<<"plane ajouté ! ";
    }

};
void Database::closedb(){
    my_db.close();
}
