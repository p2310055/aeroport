#include <ctime>
#include "headers/Date.h"
using namespace std;

// Date d'aujourd'hui
// Obtenir le temps actuel
time_t currentTime = std::time(nullptr);
// Utiliser la fonction localtime pour convertir le temps en une structure tm
tm *localTime = std::localtime(&currentTime);
Date::Date() {
    // initialisation à la date d'aujourd'hui

    heure = 0;
    jour = localTime->tm_mday;
    mois = (localTime->tm_mon + 1);
    annee = (localTime->tm_year + 1900);
}
Date::Date(unsigned int _jour, unsigned int _mois, unsigned int _annee, unsigned int _heure) {
    jour = _jour;
    mois = _mois;
    annee = _annee;
    heure = _heure;
}

Date::Date(unsigned int _jour, unsigned int _mois, unsigned int _annee) {
    jour = _jour;
    mois = _mois;
    annee = _annee;
    heure = 0;
}

void Date::setJour(const unsigned int nvJour) {
    jour = nvJour;
}
void Date::setMois(const unsigned int nvMois) {
    mois = nvMois;
}
void Date::setAnnee(const unsigned int nvAnnee) {
    annee = nvAnnee;
}

void Date::setHeure(const unsigned int nvHeure) {
    heure = nvHeure;
}

unsigned int Date::getJour() const {
    return jour;
}
unsigned int Date::getMois() const {
    return mois;
}
unsigned int Date::getAnnee() const {
    return annee;
}

unsigned int Date::getHeure() const {
    return heure;
}
QString Date::qdatetoString()const{
    return QString("%1/%2/%3").arg(jour).arg(mois).arg(annee);

}
string Date::dateToString(const Date d1)const{
    string a = to_string((d1.getJour()));
    string b = to_string((d1.getMois()));
    string c = to_string((d1.getAnnee()));

    string Textdate = a+"/"+b+"/"+c;
    return Textdate;
}
