#include "headers/pageac.h"
#include "ui_pageac.h"
#include <QTextEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <iostream>
#include <QTextEdit>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QSqlQuery>
#include "headers/database.h"
//#include "headers/editcustomer.h"

using namespace std;
bool edited = false;
pageac::pageac(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::pageac)
{
    ui->setupUi(this);


}

pageac::~pageac()
{
    delete ui;
}

void pageac::on_pushButton_2_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}


void pageac::on_pushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}


void pageac::on_pushButton_3_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);


}



void pageac::on_pushButton_5_clicked()
{

    custinfo = new customerInfo(this);
    custinfo->show();

}


void pageac::on_pushButton_test_clicked()
{
    // recuperation de tableView
    QStandardItemModel *model = qobject_cast<QStandardItemModel*>(ui->tableView->model());
    if (!model) {
        model = new QStandardItemModel(this);
        ui->tableView->setModel(model);
    }
    model->clear();

    // creation de colones
    model->setHorizontalHeaderItem(0,new QStandardItem("Id"));
    model->setHorizontalHeaderItem(1,new QStandardItem("First name"));
    model->setHorizontalHeaderItem(2,new QStandardItem("Last name"));
    model->setHorizontalHeaderItem(3,new QStandardItem("Birth date"));
    model->setHorizontalHeaderItem(4,new QStandardItem("Mail"));
    model->setHorizontalHeaderItem(5,new QStandardItem("Phone"));
    model->setHorizontalHeaderItem(6,new QStandardItem("Regestration_date"));
    model->setHorizontalHeaderItem(7,new QStandardItem("Fedility_points"));
    model->setHorizontalHeaderItem(8,new QStandardItem("Actions"));
    //recuperation des données :
    Database mydb;
    QSqlQuery qry;
    qry.prepare("SELECT * FROM customer");
    QString customerId;
    QString customerName;
    QString customerforName;
    QString customerBirthdate;
    QString customerEmail;
    QString customerPhone;
    QString customerRegestrationdate;
    QString customerFedilitypoints;
    if(qry.exec()) {
        while (qry.next()) {
            customerId = qry.value(0).toString();
            customerName = qry.value(1).toString();
            customerforName = qry.value(2).toString();
            customerBirthdate = qry.value(3).toString();
            customerEmail = qry.value(4).toString();
            customerPhone = qry.value(5).toString();
            customerRegestrationdate = qry.value(6).toString();
            customerFedilitypoints = qry.value(7).toString();


            QList<QStandardItem*> newRow;
            newRow << new QStandardItem(customerId) << new QStandardItem(customerName) << new QStandardItem(customerforName) << new QStandardItem(customerBirthdate)
                   << new QStandardItem(customerEmail) << new QStandardItem(customerPhone) << new QStandardItem(customerRegestrationdate)
                   << new QStandardItem(customerFedilitypoints);
            ui->tableView->resizeColumnToContents(4);
            //model->appendRow(newRow);
            //QPushButton *editButton = new QPushButton("Edit Client");
            for(QStandardItem* Item:newRow){
                Item->setEditable(false);
            }
            // Création du bouton "Edit Client"
            QPushButton *editButton = new QPushButton("Edit Client");
            // Connectez le clic du bouton à une slot ou une fonction qui gérera l'édition du client
            connect(editButton, &QPushButton::clicked, [=]() {
                if(!edited){
                    editButton->setText("Submit");
                    QModelIndexList selectedIndexes = ui->tableView->selectionModel()->selectedIndexes();
                    if (!selectedIndexes.isEmpty()) {
                        int row = selectedIndexes.first().row();

                        // Activez le mode d'édition pour chaque élément de la ligne
                        for (int column = 1; column < model->columnCount(); ++column) {
                            QModelIndex index = model->index(row, column);
                            QStandardItem *item = model->itemFromIndex(index);
                            if (item)
                                item->setEditable(true);
                        }
                        ////////////////////////////////////////////////////

                        ///////////////////////////////////////////////////////////////////////////////////
                    }
                    edited = true;
                    /////////////////////////////////////////ù
                    ///

                    /// /////////////////////////////////////////

                }else {
                    editButton->setText("Edit Client");
                    QModelIndexList selectedIndexes = ui->tableView->selectionModel()->selectedIndexes();
                    if (!selectedIndexes.isEmpty()) {
                        int row = selectedIndexes.first().row();

                        // Désactivez le mode d'édition pour chaque élément de la ligne
                        for (int column = 1; column < model->columnCount(); ++column) {
                            QModelIndex index = model->index(row, column);
                            QStandardItem *item = model->itemFromIndex(index);
                            if (item)
                                item->setEditable(false); // Remettez le mode en lecture seule
                        }
                        QString newCustomerName = model->item(row, 1)->text();
                        QString newCustomerforName = model->item(row, 2)->text();
                        QString newCustomerBirthdate = model->item(row, 3)->text();
                        QString newCustomerEmail = model->item(row, 4)->text();
                        QString newCustomerPhone = model->item(row, 5)->text();
                        QString newCustomerRegestrationdate = model->item(row, 6)->text();
                        QString newCustomerFedilitypoints = model->item(row, 7)->text();
                        QSqlQuery updateQuery;

                        updateQuery.prepare("UPDATE customer SET first_name=?, last_name=?,"
                                            "birth_date=?,email=?,"
                                            "phone=?,regestration_date=?,"
                                            "fidelity_score=? WHERE id=?");
                        updateQuery.addBindValue(newCustomerName);
                        updateQuery.addBindValue(newCustomerforName);
                        updateQuery.addBindValue(newCustomerBirthdate);
                        updateQuery.addBindValue(newCustomerEmail);
                        updateQuery.addBindValue(newCustomerPhone);
                        updateQuery.addBindValue(newCustomerRegestrationdate);
                        updateQuery.addBindValue(newCustomerFedilitypoints);
                        updateQuery.addBindValue(customerId); // La clé primaire reste inchangée
                        if (updateQuery.exec()) {
                            // Affichez un message de succès ou effectuez d'autres actions nécessaires
                            qDebug() << "Modification sauvegardée avec succès";
                        } else {
                            // Gérez les erreurs d'exécution de la requête SQL
                            qDebug() << "Erreur lors de la sauvegarde de la modification:" << updateQuery.lastError().text();
                        }
                    }
                    edited = false; // Réinitialisez la variable edited

                    //////////////////////////////////////////////////////////////////// base de donné

                    /// /////////////////////////////////////////////////////////////////////////////
                }

            });
            // Définissez le style pour l'en-tête de colonne
            QString headerStyle = "QHeaderView::section {"
                                  "background-color: #9CAFAA;" // Couleur jaune pour l'en-tête de colonne
                                  "}";

            // Appliquez le style à l'en-tête de colonne
            ui->tableView->horizontalHeader()->setStyleSheet(headerStyle);
            ui->tableView->verticalHeader()->setStyleSheet(headerStyle);


            // Ajout du bouton à la liste des items de la nouvelle ligne
            newRow << new QStandardItem();
            model->appendRow(newRow);
            QModelIndex index = model->index(model->rowCount() - 1, model->columnCount() - 1);
            ui->tableView->setIndexWidget(index, editButton);

            ;}


        // Créer une nouvelle ligne

    }
    mydb.closedb();

}

bool editedFlight = false;
void pageac::on_pushButton_6_clicked()
{

    // ui->tableWidget->se
    QStandardItemModel *model = qobject_cast<QStandardItemModel*>(ui->tableView_2->model());
    if (!model) {
        model = new QStandardItemModel(this);
        ui->tableView_2->setModel(model);
    }
    model->clear();

    // creation de colones
    model->setHorizontalHeaderItem(0,new QStandardItem("Id"));
    model->setHorizontalHeaderItem(1,new QStandardItem("departure"));
    model->setHorizontalHeaderItem(2,new QStandardItem("arrival"));
    model->setHorizontalHeaderItem(3,new QStandardItem("date_departure"));
    model->setHorizontalHeaderItem(4,new QStandardItem("date_arrival"));
    model->setHorizontalHeaderItem(5,new QStandardItem("price"));
    model->setHorizontalHeaderItem(6,new QStandardItem("PlaneType"));
    model->setHorizontalHeaderItem(7,new QStandardItem("class"));
    model->setHorizontalHeaderItem(8,new QStandardItem("Action"));

    /////
    Database mydb;
    QSqlQuery qry;
    qry.prepare("SELECT * FROM flight");
    QString idF;
    QString departure;
    QString arrival;
    QString date_departure;
    QString date_arrival;
    QString price;
    QString planeType;
    QString fclass;

    if(qry.exec()) {
        while (qry.next()) {
            idF = qry.value(0).toString();
            departure = qry.value(1).toString();
            arrival = qry.value(2).toString();
            date_departure = qry.value(3).toString();
            date_arrival = qry.value(4).toString();
            price = qry.value(5).toString();
            planeType = qry.value(6).toString();
            fclass = qry.value(7).toString();




            QList<QStandardItem*> newRow;
            newRow << new QStandardItem(idF) << new QStandardItem(departure) << new QStandardItem(arrival)
                   << new QStandardItem(date_departure) << new QStandardItem(date_arrival) << new QStandardItem(price)
                   << new QStandardItem(planeType)<< new QStandardItem(fclass);
            //ui->tableView->resizeColumnToContents(4);
            //model->appendRow(newRow);
            //QPushButton *editButton = new QPushButton("Edit Client");
            for(QStandardItem* Item:newRow){
                Item->setEditable(false);
            }
            // Création du bouton "Edit Client"
            QPushButton *editButton = new QPushButton("Edit flight");
            // Connectez le clic du bouton à une slot ou une fonction qui gérera l'édition du client
            connect(editButton, &QPushButton::clicked, [=]() {
                if(!editedFlight){
                    editButton->setText("Submit");
                    QModelIndexList selectedIndexes = ui->tableView_2->selectionModel()->selectedIndexes();
                    if (!selectedIndexes.isEmpty()) {
                        int row = selectedIndexes.first().row();

                        // Activez le mode d'édition pour chaque élément de la ligne
                        for (int column = 1; column < model->columnCount(); ++column) {
                            QModelIndex index = model->index(row, column);
                            QStandardItem *item = model->itemFromIndex(index);
                            if (item)
                                item->setEditable(true);
                        }
                        ////////////////////////////////////////////////////

                        ///////////////////////////////////////////////////////////////////////////////////
                    }
                    editedFlight = true;
                    /////////////////////////////////////////ù
                    ///

                    /// /////////////////////////////////////////

                }else {
                    editButton->setText("Edit Client");
                    QModelIndexList selectedIndexes = ui->tableView_2->selectionModel()->selectedIndexes();
                    if (!selectedIndexes.isEmpty()) {
                        int row = selectedIndexes.first().row();

                        // Désactivez le mode d'édition pour chaque élément de la ligne
                        for (int column = 1; column < model->columnCount(); ++column) {
                            QModelIndex index = model->index(row, column);
                            QStandardItem *item = model->itemFromIndex(index);
                            if (item)
                                item->setEditable(false); // Remettez le mode en lecture seule
                        }
                        QString newdeparture = model->item(row, 1)->text();
                        QString newarrival = model->item(row, 2)->text();
                        //QString newplaneId = model->item(row, 3)->text();
                        QString newdate_departure = model->item(row, 3)->text();
                        QString newdate_arrival = model->item(row, 4)->text();
                        QString newprice = model->item(row, 5)->text();
                        QString newplaneType = model->item(row, 6)->text();
                        QString newfclass = model->item(row, 7)->text();
                        QSqlQuery updateQuery;
                        //idF,departure,arival,date_departure,date_arrival,price,plane_type,class
                        // updateQuery.prepare("UPDATE flight SET departure=?, arival=?,"
                        //                     "date_departure=?,"
                        //                     "date_arrival=?,price=?,"
                        //                     "plane_type=?,class=?, WHERE idF=?");
                        updateQuery.prepare("UPDATE flight SET departure=?, arival=?,"
                                            "date_departure=?,"
                                            "date_arrival=?,price=?,"
                                            "plane_type=?,class=? WHERE idF=?");

                        updateQuery.addBindValue(newdeparture);
                        updateQuery.addBindValue(newarrival);
                        //updateQuery.addBindValue(newplaneId);
                        updateQuery.addBindValue(newdate_departure);
                        updateQuery.addBindValue(newdate_arrival);
                        updateQuery.addBindValue(newprice);
                        updateQuery.addBindValue(newplaneType);
                        updateQuery.addBindValue(newfclass);
                        updateQuery.addBindValue(idF); // La clé primaire reste inchangée
                        if (updateQuery.exec()) {
                            // Affichez un message de succès ou effectuez d'autres actions nécessaires
                            qDebug() << "Modification sauvegardée avec succès";
                        } else {
                            // Gérez les erreurs d'exécution de la requête SQL
                            qDebug() << "Erreur lors de la sauvegarde de la modification:" << updateQuery.lastError().text();
                        }
                    }
                    editedFlight = false; // Réinitialisez la variable edited

                    //////////////////////////////////////////////////////////////////// base de donné

                    /// /////////////////////////////////////////////////////////////////////////////
                }

            });
            // Définissez le style pour l'en-tête de colonne
            QString headerStyle = "QHeaderView::section {"
                                  "background-color: #9CAFAA;" // Couleur jaune pour l'en-tête de colonne
                                  "}";

            // Appliquez le style à l'en-tête de colonne
            ui->tableView_2->horizontalHeader()->setStyleSheet(headerStyle);
            ui->tableView_2->verticalHeader()->setStyleSheet(headerStyle);


            // Ajout du bouton à la liste des items de la nouvelle ligne
            newRow << new QStandardItem();
            model->appendRow(newRow);
            QModelIndex index = model->index(model->rowCount() - 1, model->columnCount() - 1);
            ui->tableView_2->setIndexWidget(index, editButton);

            ;}


        // Créer une nouvelle ligne

    }
     mydb.closedb();

}


void pageac::on_pushButton_7_clicked()
{
    addflightpage = new addFlight(this);
    addflightpage->show();
}


void pageac::on_pushButton_8_clicked()
{

    // qDebug()<<"hhhh";
    // Récupération du modèle de la tableView
    QStandardItemModel *model = qobject_cast<QStandardItemModel*>(ui->tableView_reservation->model());
    if (!model) {
        model = new QStandardItemModel(this);
        ui->tableView_reservation->setModel(model);
    }
    model->clear();

    // Création des colonnes
    model->setHorizontalHeaderItem(0, new QStandardItem("IdP"));
    model->setHorizontalHeaderItem(1, new QStandardItem("name"));
    model->setHorizontalHeaderItem(2, new QStandardItem("type"));
    model->setHorizontalHeaderItem(3, new QStandardItem("state"));
    model->setHorizontalHeaderItem(4, new QStandardItem("seats"));

    // Connexion à la base de données
    Database mydb;
    QSqlQuery qry;
    qry.prepare("SELECT * FROM plane");

    if(qry.exec()) {
        while (qry.next()) {
            QString idP = qry.value(0).toString();
            QString name = qry.value(1).toString();
            QString type = qry.value(2).toString();
            QString state = qry.value(3).toString();
            QString total_Seats = qry.value(4).toString();

            QList<QStandardItem*> newRow;
            newRow << new QStandardItem(idP) << new QStandardItem(name) << new QStandardItem(type)
                   << new QStandardItem(state)<< new QStandardItem(total_Seats);
            model->appendRow(newRow);
        }
    } else {
        qDebug() << "Erreur lors de l'exécution de la requête SQL:" << qry.lastError().text();
    }

    // Fermeture de la base de données
    }


void pageac::on_pushButton_9_clicked()
{
    addpl = new addPlane(this);
    addpl->show();
}


void pageac::on_pushButton_4_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
    Database mydb;
    QSqlQuery query1;
    // Préparez la requête SQL pour compter le nombre de lignes dans la table "plane"
    query1.prepare("SELECT COUNT(*) FROM plane");

    // Exécutez la requête
    if(query1.exec()) {
        // Si la requête réussit, récupérez le résultat
        if(query1.next()) {
            // Utilisez value(0) pour obtenir la valeur de la première colonne dans le résultat (COUNT(*) renvoie une seule colonne)
            QString rowCount = query1.value(0).toString();
            qDebug() << "Nombre de lignes dans la table plane : " << rowCount;
            ui->label_plane->setText(rowCount);

        }
    } else {
        // En cas d'échec de l'exécution de la requête, affichez une erreur
        qDebug() << "Erreur lors de l'exécution de la requête : " << query1.lastError().text();
    }
    QSqlQuery query2;
    // Préparez la requête SQL pour compter le nombre de lignes dans la table "plane"
    query2.prepare("SELECT COUNT(*) FROM customer");

    // Exécutez la requête
    if(query2.exec()) {
        // Si la requête réussit, récupérez le résultat
        if(query2.next()) {
            // Utilisez value(0) pour obtenir la valeur de la première colonne dans le résultat (COUNT(*) renvoie une seule colonne)
            QString CurowCount = query2.value(0).toString();
            qDebug() << "Nombre de lignes dans la table plane : " << CurowCount;
            ui->label_plane_cust->setText(CurowCount);

        }
    } else {
        // En cas d'échec de l'exécution de la requête, affichez une erreur
        qDebug() << "Erreur lors de l'exécution de la requête : " << query2.lastError().text();
    }
    QSqlQuery query3;
    // Préparez la requête SQL pour compter le nombre de lignes dans la table "plane"
    query3.prepare("SELECT COUNT(*) FROM flight");

    // Exécutez la requête
    if(query3.exec()) {
        // Si la requête réussit, récupérez le résultat
        if(query3.next()) {
            // Utilisez value(0) pour obtenir la valeur de la première colonne dans le résultat (COUNT(*) renvoie une seule colonne)
            QString CurowCount = query3.value(0).toString();
            qDebug() << "Nombre de lignes dans la table plane : " << CurowCount;
            ui->label_plane_fli->setText(CurowCount);

        }
    } else {
        // En cas d'échec de l'exécution de la requête, affichez une erreur
        qDebug() << "Erreur lors de l'exécution de la requête : " << query3.lastError().text();
    }
}

