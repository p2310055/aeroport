#include "headers/Ticket.h"

Ticket::Ticket(const string &_flightId, const unsigned int &_seats) {
    flightId = _flightId;
    seats = _seats;
}

string Ticket::getFlightId() const {
    return flightId;
}

unsigned int Ticket::getSeats() const {
    return seats;
}

